1. In root directory Create copy of .env.template file and call it .env
2. Fill all parameters on .env file and add this file to .gitignore
3. Check 'config' directory - set default values on auth.py file, routing.py file and test_run.py file
4. Set driver, which you need for in driver.py file on the root directory