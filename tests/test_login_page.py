import time

import pytest

from pages.auth.login_page import LogInPage
from pages.dashboard_page import DashboardPage
from selenium.webdriver.remote.webdriver import WebDriver
import config
import driver

@pytest.fixture
def login_page(webdriver: WebDriver) -> LogInPage:
    """Initialization"""
    return LogInPage.open_from_url(webdriver=webdriver, path=LogInPage.path)


def test_sign_in(
        login_page: LogInPage
):
    print(config.auth.DEFAULT_LOGIN)
    toolbar = login_page.submit_form(
        site_code="admin",
        username=config.auth.DEFAULT_LOGIN,
        password=config.auth.DEFAULT_PASSWORD
    )
    time.sleep(10)
