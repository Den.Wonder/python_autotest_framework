from selenium.webdriver.remote.webdriver import WebDriver
import pytest
from _pytest.config import Config
from _pytest.fixtures import SubRequest

import driver


@pytest.fixture
def webdriver(
        request: SubRequest,
) -> WebDriver:
    """Initialize webdriver. Primary for not logged-in user."""
    webdriver: WebDriver = driver.start()
    return webdriver
