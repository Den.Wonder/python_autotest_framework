import os
import typing

import driver
from pages.base_page import WebAppPage
from selenium.webdriver.remote.webdriver import WebDriver

from webclient.page_object import locators


class DashboardPage(WebAppPage):
    def __init__(self, webdriver: WebDriver, *args, **kwargs):
        super().__init__(webdriver, *args, **kwargs)
        self.toolbar = self.init_element(
            locator=locators.IdLocator(
                query='appbar'
            ),
        )