import os
import typing

import driver
from pages.base_page import WebAppPage
from selenium.webdriver.remote.webdriver import WebDriver

from webclient.page_object import locators
from .forgot_page import ForgotPage
from ..dashboard_page import DashboardPage
import time

class LogInPage(WebAppPage):
    path = "login"

    def __init__(self, webdriver: WebDriver, *args, **kwargs):
        super().__init__(webdriver, *args, **kwargs)
        self.site_code_input = self.init_element(
            locator=locators.NameLocator(
                query="sitecode",
            ),
        )
        self.user_name_input = self.init_element(
            locator=locators.NameLocator(
                query="login"
            ),
        )
        self.password_input = self.init_element(
            locator=locators.NameLocator(
                query="password"
            ),
        )
        self.forgot_password_button = self.init_element(
            locator=locators.ElementWithExactTextLocator(
                text="Forgot Password",
            ),
        )
        self.login_button = self.init_element(
            locator=locators.ClassLocator(
                query="login__button"
            ),
        )
        self.loader_button = self.init_element(
            locator=locators.ClassLocator(
                query="v-btn__loader"
            )
        )

    def submit(self):
        self.login_button.click()
        self.loader_button.wait_until_invisible()

    def submit_form(
            self,
            site_code: str,
            username: str,
            password: str
    ) -> typing.Union[DashboardPage, "LogInPage"]:
        login_url = self.current_url
        self.site_code_input.fill(text=site_code)
        self.user_name_input.fill(text=username)
        self.password_input.fill(text=password)
        self.submit()
        if login_url == self.current_url:
            return self
        return DashboardPage(self.webdriver)