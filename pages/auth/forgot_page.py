import os
import typing

import driver
from pages.base_page import WebAppPage
from selenium.webdriver.remote.webdriver import WebDriver

from webclient.page_object import locators


class ForgotPage(WebAppPage):
    def __init__(self, webdriver: WebDriver, *args, **kwargs):
        super().__init__(webdriver, *args, **kwargs)
        self.site_code_input = self.init_element(
            locator=locators.NameLocator(
                query="sitecode",
            ),
        )
        self.user_name_input = self.init_element(
            locator=locators.NameLocator(
                query="userName"
            ),
        )
        self.login_button = self.init_element(
            locator=locators.ElementWithExactTextLocator(
                text="Login"
            ),
        )
        self.submit_button = self.init_element(
            locator=locators.ElementWithExactTextLocator(
                text="Submit"
            ),
        )

