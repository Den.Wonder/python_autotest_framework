import driver
import time


# browser = webdriver.Chrome()
browser = driver.start()


try:
    browser.get('https://www.google.com/')
    time.sleep(5)
    browser.refresh()


finally:
    time.sleep(10)
    browser.quit()
