from webclient.page_object import Page as BasePage
from webclient.page_object import locators

class WebAppPage(BasePage):
    """Base representation of page for uSummit.

    It contains common shortcuts.

    """
