from . import env

# Setting default valid CREDs.
DEFAULT_LOGIN = env.get('DEFAULT_LOGIN', '')
DEFAULT_PASSWORD = env.get('DEFAULT_PASSWORD', '')
DEFAULT_EMAIL = env.get('DEFAULT_EMAIL', '')

# Default user object. Improve it, if you need to.
USERS = {
    'default': {
        'password': DEFAULT_PASSWORD,
        'email': DEFAULT_EMAIL,
        'login': DEFAULT_LOGIN
    },
}


# Define default user object in environment
def get_user(user):
    _user = USERS.get(user)
    if not _user:
        return ValueError('Undefined user name')
    return _user
