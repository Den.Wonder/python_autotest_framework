import os
from . import env


ONE_SESSION = env.get_bool('ONE_SESSION', 'False')
BROWSER_LOGS = env.get_bool('BROWSER_LOGS', 'False')

# Define platforms
PLATFORM = env.get('PLATFORM', 'WEB')
# shortcuts
IS_WEB = PLATFORM == 'WEB'
IS_MOBILE = PLATFORM in ('IOS', 'ANDROID')
IS_ANDROID = PLATFORM == 'ANDROID'
IS_IOS = PLATFORM == 'IOS'

# def default browser for driver select
# Chrome | Firefox | Edge | Safari
WEB_BROWSER = env.get('WEB_BROWSER', 'Chrome')

# *** For feature with mobile devices test framework: ***
# def default appium server. By default appium launch server on port 4723
APPIUM_SERVER = env.get('APPIUM_SERVER', 'http://localhost:4723/wd/hub')
# def default app names in default directory. just enter .app and .apk file names
APP_NAME = {
    'IOS': env.get('IOS_APP_NAME', ''),
    'ANDROID': env.get('ANDROID_APP_NAME', '')
}.get(PLATFORM)

# Define default mobile app directory. In this case is '/mobile_app' directory of project
_DEFAULT_APP_PATH = str(os.path.join(os.getcwd(), fr"mobile_app/{APP_NAME}"))
MOBILE_APP = env.get('MOBILE_APP', _DEFAULT_APP_PATH) if IS_MOBILE else None

# Settings of mobile platform versions and device names.
ANDROID_VERSION = env.get('ANDROID_VERSION', '11.0')
ANDROID_DEVICE_NAME = env.get('ANDROID_DEVICE_NAME', '')

IOS_VERSION = env.get('IOS_VERSION', '13.4')
IOS_DEVICE_NAME = env.get('IOS_DEVICE_NAME', '')

APPIUM_TIMEOUT = int(env.get('APPIUM_TIMEOUT', 60))

