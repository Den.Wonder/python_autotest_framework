from . import env

# define default URLS of web application. This is optional, but it's much better
# than define it inside code and change everywhere, if you'll need to change it
LOGIN_PAGE_URL = env.get('LOGIN_PAGE_URL', 'https://admin.dev.medic-clipboard.com/login')

MAIN_PAGE_URL = env.get('MAIN_PAGE_URL', 'https://admin.dev.medic-clipboard.com/')

ROOT = env.get('APP_HOST', 'https://admin.dev.medic-clipboard.com/')