from contextlib import contextmanager

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait

import driver
from . import exceptions
from . import locators
from . import wait_conditions
from .element import Element

# configured_web_driver = driver.start()


class WebView:
    """Base representation of web object."""

    def __init__(
            self,
            app_root: str,
            webdriver: WebDriver,
            wait_timeout: int,
            poll_frequency=float(0),
            **kwargs,
    ):
        """Set up webdriver and WebDriverWait."""
        self.webdriver: WebDriver = webdriver
        self.app_root: str = app_root
        self.wait_timeout: int = wait_timeout
        self.wait = WebDriverWait(
            driver=webdriver,
            timeout=wait_timeout,
            poll_frequency=poll_frequency,
        )

    def init_element(self, locator: locators.Locator) -> Element:
        """Initializing only one element instances."""
        return Element(self, locator=locator)

    def init_elements(self, locator: locators.XPathLocator) -> list[Element]:
        """Initializing multiple elements instances via one xpath locator."""
        assert isinstance(locator, locators.XPathLocator), "Only supports Xpath locators!"
        elements_count = len(self._get_elements(locator=locator))
        return [
            self.init_element(
                locator=locators.XPathLocator(query=f"({locator.query})[{index + 1}]"),
            )
            for index in range(elements_count)
        ]

    def _get_element(self, locator: locators.Locator, only_visible=True) -> WebElement:
        """Get one WebElement from page via locator."""
        if only_visible:
            self.wait_until_visible(locator=locator)
        return self.webdriver.find_element(*locator)

    def _get_elements(self, locator: locators.Locator) -> list[WebElement]:
        """Get multiple webelements from page via locator."""
        return self.webdriver.find_elements(*locator)

    def wait_until_visible(self, locator: locators.Locator):
        """Wait until element matching locator becomes visible."""
        try:
            self.wait.until(expected_conditions.visibility_of_element_located(locator))
        except TimeoutException:
            raise exceptions.ElementIsNotVisibleError(
                f"Unable to find {locator} in {self.wait_timeout} seconds!",
            )

    def wait_until_invisible(self, locator: locators.Locator):
        """Wait until element matching locator becomes invisible."""
        try:
            self.wait.until(expected_conditions.invisibility_of_element_located(locator))
        except TimeoutException:
            raise exceptions.ElementIsNotVisibleError(
                f"{locator} is still visible in {self.wait_timeout} seconds!",
            )

    def wait_until_clickable(self, locator: locators.Locator):
        """Wait until element matching locator becomes clickable."""
        try:
            self.wait.until(expected_conditions.element_to_be_clickable(locator))
        except TimeoutException:
            raise exceptions.ElementIsNotClickableError(
                f"{locator} is not clickable after {self.wait_timeout} seconds!",
            )

    def wait_until_text_is_in_element(self, text: str, locator: locators.Locator):
        try:
            self.wait.until(expected_conditions.text_to_be_present_in_element(locator, text))
        except TimeoutException:
            raise exceptions.TextIsNotInElementError(
                f"{locator} doesn't have `{text}` after {self.wait_timeout} seconds!",
            )

    def scroll_to(self, target: WebElement):
        """Scroll page to target"""
        self.webdriver.execute_script("arguments[0].scrollIntoView();", target)

    def scroll_to_top(self):
        self.webdriver.execute_script("window.scrollBy(0, -document.body.scrollHeight)")

    def scroll_to_bottom(self):
        self.webdriver.execute_script("window.scrollBy(0, document.body.scrollHeight)")

    def execute_javascript(self, script: str, *args):
        """Execute simple javascript."""
        self.webdriver.execute_script(script, *args)

    def switch_to_default(self):
        """Switch focus of webdriver to default content."""
        self.webdriver.switch_to.default_content()

    def switch_to_iframe(self, locator: locators.Locator):
        """Switch focus of webdriver to iframe."""
        self.webdriver.switch_to.frame(self._get_element(locator))

    @contextmanager
    def iframe_switcher_manager(self, locator: locators.Locator):
        """Context manager for interacting with iframes."""
        self.switch_to_iframe(locator=locator)
        yield
        self.switch_to_default()

    @property
    def current_url(self) -> str:
        return self.webdriver.current_url

    def wait_until_url_contains(self, url: str):
        """Wait until browser's url contains input url."""
        try:
            self.wait.until(expected_conditions.url_contains(url))
        except TimeoutException:
            raise exceptions.UrlDoesContainError(
                f"Url doesn't contain `{url}` in {self.wait_timeout} seconds!",
            )

    def wait_until_url_not_contains(self, url: str):
        """Wait until browser's url doesn't not contains input url."""
        try:
            self.wait.until(wait_conditions.url_not_matches(url))
        except TimeoutException:
            raise exceptions.UrlDoesContainError(
                f"Url does contain `{url}` in {self.wait_timeout} seconds!",
            )

    def wait_until_url_changes(self, url: str = None):
        """Wait until url changes."""
        url = url if url else self.current_url
        try:
            self.wait.until(expected_conditions.url_changes(url))
        except TimeoutException:
            raise exceptions.UrlDidNotChangedError(
                f"Url didn't changed from {url} in {self.wait_timeout} seconds!",
            )
