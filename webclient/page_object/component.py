from .web_view import WebView


class Component(WebView):
    """Basic representation of page component.
    Component contains elements of page and methods for page
    component manipulation, but as a separate entity, that can be
    reused for different pages with common elements.
    """

    def __init__(self, page):
        super().__init__(
            app_root=page.app_root,
            webdriver=page.webdriver,
            wait_timeout=page.wait_timeout
        )
        self.page = page
