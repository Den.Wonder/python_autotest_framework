import os
from .web_view import WebView
import config
from selenium.webdriver.remote.webdriver import WebDriver


class Page(WebView):
    """Basic representation of web page.

    It contains elements and components of WEB page.
    Also, it contains methods for WEB page manipulation.

    """

    APP_ROOT = f"{os.environ['APP_HOST']}"

    def __init__(
            self,
            webdriver: WebDriver,
            app_root: str = APP_ROOT,
            wait_timeout=int(os.environ.get("BROWSER_WAIT", 5)),
            poll_frequency=float(os.environ.get("BROWSER_POLL_FREQUENCY", 0)),
            **kwargs,
    ):
        super().__init__(
            app_root=app_root,
            webdriver=webdriver,
            wait_timeout=wait_timeout,
            poll_frequency=poll_frequency,
            **kwargs,
        )

    def navigate(self, url: str):
        """Navigate absolute URL"""
        self.webdriver.get(url)

    def navigate_relative(self, relative_url: str = "/"):
        """Navigate to URL relative to application root

        Args:
            relative_url (str): Relative URL
        """
        self.webdriver.get(f"{self.app_root}{relative_url}")

    def refresh(self):
        """Refresh web page."""
        return self.webdriver.refresh()

    @classmethod
    def open(
            cls,
            webdriver: WebDriver,
            app_root: str = APP_ROOT,
            *args,
            **kwargs
    ):
        page = cls(webdriver=webdriver, app_root=app_root, *args, **kwargs)
        page.navigate_relative()
        return page

    @classmethod
    def open_from_url(
            cls,
            webdriver: WebDriver,
            path: str,
            app_root: str = APP_ROOT,
            *args,
            **kwargs
    ):
        page = cls(webdriver=webdriver, app_root=app_root, *args, **kwargs)
        page.navigate_relative(relative_url=path)
        return page
