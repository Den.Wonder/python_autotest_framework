# Custom waits conditions
import re


class url_not_matches:
    """An condition that checks that url doesn't match pattern."""

    def __init__(self, pattern):
        self.pattern = pattern

    def __call__(self, driver):
        match = re.search(self.pattern, driver.current_url)
        return match is None
