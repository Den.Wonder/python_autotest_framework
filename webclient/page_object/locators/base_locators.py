from selenium.webdriver.common.by import By


class Locator:
    """Base locator for looking for elements in page."""

    _ALLOWED_LOCATORS = (
        By.ID,
        By.XPATH,
        By.LINK_TEXT,
        By.PARTIAL_LINK_TEXT,
        By.NAME,
        By.TAG_NAME,
        By.CLASS_NAME,
        By.CSS_SELECTOR,
    )

    def __init__(self, by: str, query: str):
        """Init locator."""
        if by not in self._ALLOWED_LOCATORS:
            raise ValueError(f"No valid `by` found -> `{by}`")
        self.by: str = by
        self.query: str = query

    def __iter__(self):
        """Unpack locator"""
        return iter((self.by, self.query))

    def __repr__(self):
        return f"Locator<By `{self.by}`: Query `{self.query}`>"


class IdLocator(Locator):
    """Locator to looking for elements in page by Id."""

    def __init__(self, query: str):
        """Init locator."""
        super().__init__(by=By.ID, query=query)


class XPathLocator(Locator):
    """Locator to looking for elements in page by XPath."""

    def __init__(self, query: str):
        """Init locator."""
        super().__init__(by=By.XPATH, query=query)

    def __add__(self, other: "XPathLocator"):
        """Provide ability to implement nested XPath locators"""
        return XPathLocator(query=self.query + other.query)


class LinkTextLocator(Locator):
    """Locator to looking for elements in page by link text."""

    def __init__(self, query: str, is_partial: bool = False):
        """Init locator."""
        super().__init__(
            by=By.LINK_TEXT if not is_partial else By.PARTIAL_LINK_TEXT,
            query=query,
        )


class NameLocator(Locator):
    """Locator to looking for elements in page by name."""

    def __init__(self, query: str):
        """Init locator."""
        super().__init__(by=By.NAME, query=query)


class TagNameLocator(Locator):
    """Locator to looking for elements in page by tag name."""

    def __init__(self, query: str):
        """Init locator."""
        super().__init__(by=By.TAG_NAME, query=query)


class ClassLocator(Locator):
    """Locator to looking for elements in page by class name."""

    def __init__(self, query: str):
        """Init locator."""
        super().__init__(by=By.CLASS_NAME, query=query)


class CssLocator(Locator):
    """Locator to looking for elements in page by css selector."""

    def __init__(self, query: str):
        """Init locator."""
        super().__init__(by=By.CSS_SELECTOR, query=query)
