from .base_locators import XPathLocator


class PropertyLocator(XPathLocator):
    """Locator to looking for elements with property by XPath."""

    def __init__(
        self,
        prop: str,
        value: str,
        container: str = "*",
        extra: str = "",
    ):
        """Init locator."""
        super().__init__(query=f'//{container}[@{prop}="{value}"]{extra}')


class DataTestIdLocator(PropertyLocator):
    def __init__(
        self,
        value: str,
        container: str = "*",
        extra: str = "",
    ):
        """Init locator."""
        super().__init__(
            prop="data-testid",
            value=value,
            container=container,
            extra=extra,
        )


class PartialPropertyLocator(XPathLocator):
    """Locator to looking for elements with partial property by XPath."""

    def __init__(
        self,
        prop: str,
        value: str,
        container: str = "*",
        extra: str = "",
    ):
        """Init locator."""
        super().__init__(query=f'//{container}[contains(@{prop}, "{value}")]{extra}')


class PartialClassLocator(PartialPropertyLocator):
    """Locator to looking for elements with partial class by XPath."""

    def __init__(
        self,
        class_name: str,
        container: str = "*",
        extra: str = "",
    ):
        """Init locator."""
        super().__init__(
            prop="class",
            value=class_name,
            container=container,
            extra=extra,
        )


class ElementWithTextLocator(XPathLocator):
    """Locator to looking for elements with text by XPath.

    test() -> check only the text of element
    . -> check text of element and it's children

    """

    def __init__(self, text: str, element: str = "*", include_children: bool = False):
        """Init locator."""
        text_selector = "." if include_children else "text()"
        super().__init__(query=f'//{element}[contains({text_selector}, "{text}")]')


class ElementWithExactTextLocator(XPathLocator):
    """Locator to looking for elements with exact text by XPath."""

    def __init__(self, text: str, element: str = "*"):
        """Init locator."""
        super().__init__(query=f'//{element}[./text()="{text}"]')


class ButtonWithTextLocator(XPathLocator):
    """Locator to looking for button with text by XPath(not in children!)."""

    def __init__(self, text: str):
        """Init locator."""
        super().__init__(query=f'//button[contains(.,"{text}")]')


class InputByLabelLocator(XPathLocator):
    """Locator to looking for input with label by XPath."""

    def __init__(self, label: str):
        """Init locator."""
        super().__init__(query=f'//*[label[contains(text(), "{label}")]]/input')
