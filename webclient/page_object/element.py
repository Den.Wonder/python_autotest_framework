import os
import time

from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.select import Select

from . import locators


class Element:
    """Base representation of element on page."""

    def __init__(self, web_view, locator: locators.Locator):
        """Init page element."""
        self.web_view = web_view
        self.locator: locators.Locator = locator

    def wait_until_visible(self):
        self.web_view.wait_until_visible(locator=self.locator)

    def wait_until_invisible(self):
        self.web_view.wait_until_invisible(locator=self.locator)

    def wait_until_clickable(self):
        self.web_view.wait_until_clickable(locator=self.locator)

    def wait_until_text_is_in_element(self, text: str):
        self.web_view.wait_until_text_is_in_element(text=text, locator=self.locator)

    def get_element(self, only_visible=True) -> WebElement:
        """Get selenium instance(WebElement) of element."""
        return self.web_view._get_element(locator=self.locator, only_visible=only_visible)

    @property
    def exists_in_dom(self) -> bool:
        """Check if element is present in html, can be not visible."""
        return len(self.web_view._get_elements(locator=self.locator)) != 0

    @property
    def is_displayed(self) -> bool:
        return self.get_element(only_visible=False).is_displayed()

    @property
    def is_enabled(self) -> bool:
        return self.get_element().is_enabled()

    @property
    def is_selected(self) -> bool:
        return self.get_element().is_selected()

    def fill(
            self,
            text: str,
            times: int = 40,
            sleep_time: float = 0.,
            only_visible=True,
    ):
        """Fill element with text."""
        self.clear(only_visible=only_visible)
        input_text = str(text)
        self.send_keys(input_text, only_visible=only_visible)
        for i in range(times):
            if self.get_value() == input_text or self.get_text() == input_text:
                return
            self.send_keys(input_text, only_visible=only_visible)
            time.sleep(sleep_time)

    def clear(self, only_visible=True):
        """Clear input and it's value."""
        self.get_element(only_visible=only_visible)
        """Mac devices have not DELETE button, so we have to user backspace"""
        try:
            self.send_keys(Keys.CONTROL + "a")
            self.send_keys(Keys.DELETE)
            if self.get_value() != "":
                while self.get_value() != "":
                    self.send_keys(Keys.BACK_SPACE)
        except Exception:
            while self.get_value() != "":
                self.send_keys(Keys.BACK_SPACE)

    def send_keys(self, keys: str, only_visible=True):
        self.get_element(only_visible=only_visible).send_keys(*keys)

    def get_text(self, only_visible=True) -> str:
        """Get text from element."""
        return self.get_element(only_visible=only_visible).text

    def get_attribute(self, attribute_name: str, only_visible=True) -> str:
        """Get value of attr from element."""
        return self.get_element(only_visible=only_visible).get_attribute(name=attribute_name)

    def set_attribute(self, attribute_name: str, value: str, only_visible=True):
        element = self.get_element(only_visible=only_visible)
        self.web_view.execute_javascript(
            f"arguments[0].setAttribute('{attribute_name}',arguments[1])",
            element,
            value,
        )

    def get_value(self, only_visible=True):
        """Get value of value attr from element."""
        return self.get_attribute(attribute_name="value", only_visible=only_visible)

    def select(self, value: str, only_visible=True):
        """Perform select on element."""
        Select(self.get_element(only_visible=only_visible)).select_by_visible_text(value)

    def click(self, only_visible=True, wait_until_clickable=True, sleep_time: float = 0.0):
        """Click on element."""
        # Hack to provide to javascript time for element initialization.
        # Without this delay, click could happen early, than element will have any handlers to
        # deal with click event
        time.sleep(sleep_time)
        # Try except block help to overcome error `Element is not attached to page` error.
        attempts = 0
        max_attempts = int(os.environ.get("MAX_RETRY_ATTEMPTS", 3))
        while True:
            try:
                attempts += 1
                if wait_until_clickable:
                    self.wait_until_clickable()
                self.get_element(only_visible=only_visible).click()
                break
            except StaleElementReferenceException:
                if attempts >= max_attempts:
                    raise StaleElementReferenceException

    def scroll_to(self):
        """Scroll page until element is visible"""
        self.web_view.scroll_to(self.get_element())

    def get_value_of_css_property(self, property_name, only_visible=True) -> str:
        """Return value of a CSS property."""
        return self.get_element(only_visible=only_visible).value_of_css_property(
            property_name=property_name,
        )

    def add_debug_mark(self):
        """Set element background to red. Should be used only for debugging."""
        current_style = self.get_attribute("style")
        self.set_attribute("style", f"{current_style} background: red;")

    def remove_debug_mark(self):
        """Remove debug mark."""
        current_style = self.get_attribute("style")
        self.set_attribute("style", current_style.replace("background: red;", ""))
