from appium import webdriver as appium_driver
from selenium import webdriver as selenium_driver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager



from webdriver_manager.microsoft import EdgeChromiumDriverManager


import config


def create_web_browser_capabilities():
    if config.test_run.WEB_BROWSER == 'Chrome':
        return {
            **DesiredCapabilities.CHROME,
            'loggingPrefs': {'browser': 'ALL'}
        }
    elif config.test_run.WEB_BROWSER == 'Firefox':
        return {
            **DesiredCapabilities.FIREFOX,
            'loggingPrefs': {'browser': 'ALL'}
        }
    elif config.test_run.WEB_BROWSER == 'Edge':
        return {
            **DesiredCapabilities.EDGE,
            'loggingPrefs': {'browser': 'ALL'}
        }


def set_web_driver(caps):
    if config.test_run.WEB_BROWSER == 'Chrome':
        return selenium_driver.Chrome(
            executable_path=ChromeDriverManager().install(),
            desired_capabilities=caps
        )
    elif config.test_run.WEB_BROWSER == 'Firefox':
        return selenium_driver.Firefox(
            executable_path=GeckoDriverManager().install(),
            desired_capabilities=caps
        )



_capabilities = {
    'IOS': {
        'version': '1.8.0',
        'platformName': 'iOS',
        'platformVersion': config.test_run.IOS_VERSION,
        'deviceName': config.test_run.IOS_DEVICE_NAME,
        'app': config.test_run.MOBILE_APP,
        'noReset': False,
        'fullReset': False,
        'newCommandTimeout': config.test_run.APPIUM_TIMEOUT,
    },
    'ANDROID': {
        'version': '1.8.0',
        'platformName': 'Android',
        'platformVersion': config.test_run.ANDROID_VERSION,
        'deviceName': config.test_run.ANDROID_DEVICE_NAME,
        'app': config.test_run.MOBILE_APP,
        'noReset': False,
        'fullReset': False,
        'newCommandTimeout': config.test_run.APPIUM_TIMEOUT,
        "automationName": "UiAutomator2",
        "browserName": "",
    },
    'WEB': create_web_browser_capabilities()
}



def start():
    caps = _capabilities[config.test_run.PLATFORM]
    if config.test_run.IS_MOBILE:
        return appium_driver.Remote(
            command_executor=config.test_run.APPIUM_SERVER,
            desired_capabilities=caps
        )

    return set_web_driver(caps)


def close(driver):
    driver.close_app() if config.test_run.IS_MOBILE else driver.quit()
